# stable-polytope
This repository contains the data and scripts used for my thesis.

## Contents
- `graphs/`: folder containing the graphs and the exact formulations for the stable set polytope computed on binomial graphs.
- `src/`: folder containing the scripts used to generate the data
- `statistics/`: folder containing JSON encoded statistics for the data in `graphs`.

## Dependencies
- Python 3 
- Normaliz and the wrapper PyNormaliz
- Networkx
- Numpy
## Usage
The dependencies can be satisfied by creating the environment provided: `conda env create -f environment.yml`

To activate the environment: `conda activate stab`

### Generation of the dataset

The script to compute the formulations is
`src/gen-graphs.py`
it accepts the following flags:

`-l <int>, --min-cardinality <int>`: min number of nodes of the generated graphs. This flag is mandatory

`-u <int>, --max-cardinality <int>`: max number of nodes of the generated graphs. This flag is mandatory and must be greater than or equal to the previous.

`-m <float>, --min-density <float>`: min edge density graph for each cardinality. This must be a number between 0 and 1. The default value is 0.2.

`-M <float>, --max-density <float>`: max edge density graph for each cardinality. This must be a number between 0 and 1. The default value is 0.8.

`-s <float>, --step <float>`: increment in edge density to go from min to max density values, the default value is 0.1.

`-r <int>, --repetitions <int>`: number of graphs generated for each cardinality-density pair. Default is 1.

`-j, --human`: dump the formulations also in a human readable format.

`-o <output_dir>, --output <output_dir>`: The directory in which the dataset will be dumped. Default is `./output/`.

`-t <int>, --threads <int>`: number of threads to be used for the convex hull computation. Default is 8.

`-d, --decomposition`: enables the decomposition algorithm.

### Statistics

The script for computing the statistics on the generated datasets is `src/stats.py`.

The first argument must be one of
`classes`: computes the composition of the formulations int terms of rank, non rank, clique inequalities.

`density`: computes the mean density of the inequalities in the formulations.

`dynamic`: computes the mean dynamics of the inequalities in the formulations.

`facets`: computes the mean number of inequalities of the formulations.

`nodes`: computes the mean number of subproblems solved by branch and bound on the instances with an edge formulation. Requires `COIN-OR`'s programn `cbc` to be installed on the system, and a prior run of `src/mips.py`.

`-i <input_dir>, --input <input_dir>`: input directory generated with `src/gen-graphs.py`. This flag is mandatory

`-o <output_file>, --output <output_file>`: output file name

`-f [json|tex], --format [json|tex]`: defaults to json, it is the format of the file produced. If passed tex, it prints plots for the pgfplots latex library.
