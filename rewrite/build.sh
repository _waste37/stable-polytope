#!/bin/bash

set -xe

mkdir -p objects

cxx=clang++
cflags='-Wall -Wextra -std=c++17'

sources=('stab_gen.cpp' 'graph.cpp')
objects=()
for i in ${!sources[@]}; do
    objects[$i]="objects/${sources[$i]/%.cpp/.o}"
done

for i in ${!sources[@]}; do
    $cxx -c -o ${objects[$i]} ${sources[$i]} $cflags
done

$cxx -o stab_gen ${objects[@]} $cflags
