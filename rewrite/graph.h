#ifndef __graph_h__
#define __graph_h__

#include <map>
#include <string>
#include <vector>

using adj_map = std::map<int, std::vector<int>>;

struct graph_t
{
    graph_t();
    graph_t(std::ifstream &e_list);
    graph_t(const adj_map &adj);
    graph_t(const std::vector<std::pair<int, int>> &e_list);

    void add_edge(int u, int v);
    std::string to_str(bool indent = true);
    std::string to_e_list();
    bool contains_path(int u, int v);
    int n, m;
    adj_map adj;
};

graph_t gnp_random_graph(int n, double p);

#endif // __graph_h__
