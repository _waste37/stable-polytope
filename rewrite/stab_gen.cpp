#include <iostream>
#include <fstream>
#include "graph.h"
#include "la/la.h"

#define PROJECT_NAME "stab-gen"

int main(int argc, char **argv) 
{
    if (argc != 1) {
        std::cout << argv[0] <<  "takes no arguments.\n";
        return 1;
    }

    std::ifstream fp("test.elist");
    graph_t G(fp);
    graph_t rand = gnp_random_graph(10, 0.3);

    la::mat<float> A{
        {1, 2}, 
        {3, 4}
    };

    la::mat<float> B{
        {4, 3}, 
        {2, 1}
    };

    std::cout << "A = " << A << std::endl;
    std::cout << "B = " << B << std::endl;

    std::cout << "Test -A: " << (-A) << std::endl;
    std::cout << "Test A+B: " << (A+B) << std::endl;
    std::cout << "Test A-B: " << (A-B) << std::endl;
    std::cout << "Test A-2: " << (A-2.0f) << std::endl;
    std::cout << "Test 2-A: " << (2.0f-A) << std::endl;
    std::cout << "Test A*B: " << (A*B) << std::endl;
    std::cout << "Test A==B: " << (A==B) << std::endl;
    std::cout << "Test A+B==B+A: " << (A+B==B+A) << std::endl;

    la::vec<float> V {6,9};
    A.append_row({6,9});
    std::cout << "Test A.append_row({6,9})" << A << std::endl;
    A.remove_row(2);
    std::cout << "Test A.delete_row(2)" << A << std::endl;
    A.append_col({6,9});
    std::cout << "Test A.append_col({6,9})" << A << std::endl;
    return 0;
}
