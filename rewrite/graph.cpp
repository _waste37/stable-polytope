#include "graph.h"

#include <iostream>
#include <cassert>
#include <fstream>
#include <set>
#include <random>
#include <sstream>

graph_t::graph_t() : n(0), m(0) {}

graph_t::graph_t(std::ifstream &e_list) 
{
    assert(e_list.is_open());
    int u, v;
    std::string line;
    while (std::getline(e_list, line)) {
        std::stringstream ss;
        ss << line;
        ss >> u >> v;
        this->add_edge(u, v);
    }
}

graph_t::graph_t(const adj_map &adj)
{
    this->adj = adj;
}

graph_t::graph_t(const std::vector<std::pair<int, int>> &e_list)
{
    for (const auto &[u, v] : e_list) {
        this->add_edge(u, v);
    }
}

void graph_t::add_edge(int u, int v)
{
    m++;
    if (adj.count(u) == 0) n++;
    if (adj.count(v) == 0) n++;
    adj[u].push_back(v);
    adj[v].push_back(u);
}

std::string graph_t::to_str(bool indent) 
{
    std::stringstream ss;
    ss << "{";
    if (indent) ss << std::endl;
    for (const auto &[v, neighbors] : adj) {
        if (indent) ss << '\t';
        ss << '"' << v << "\":[";

        for (const auto &u : neighbors) {
            ss << u << ',';
        }
        ss.seekp(-1, ss.cur);
        ss << "]";
        if (indent) ss << std::endl;
    }
    ss << '}';
    return ss.str();
}

std::string graph_t::to_e_list()
{
    std::set<std::pair<int, int>> s;
    for (const auto &[v, neighbors] : adj) {
        for (const auto &u : neighbors) {
            if (u < v) {
                s.insert({u, v});
            } else {
                s.insert({v, u});
            }
        }
    }

    std::stringstream ss;
    for (const auto &[u, v] : s) {
        ss << u << ' ' << v << " {}" << std::endl;
    }
    return ss.str();
}

graph_t gnp_random_graph(int n, double p) 
{
    graph_t G;

    std::random_device rd{}; 
    std::mt19937 rng{rd()};
    std::bernoulli_distribution d(p);

    for (int u = 0; u < n; ++u) {
        for (int v = u+1; v < n; ++v) {
            if (d(rng)) {
                G.add_edge(u, v);
            }
        }
    }

    return G;
}

