#ifndef __poly_h__
#define __poly_h__

#include "mat.h"
#include "vec.h"

#include <list>
#include <set>
#include <vector>

namespace la {
    // polygon in standard form Ax >= b.
    template <typename T>
    class poly {
      public:
        poly();
        poly(const poly<T> &p_copy);
        poly(mat<T> A, vec<T> b);
        poly(mat<T> V);
        mat<T> &vertices();
        mat<T> &inequalities();
      private:
        mat<T> H;
        mat<T> V;
    };


    template <typename T>
    poly<T>::poly()
    {
    }

    template <typename T>
    poly<T>::poly(const poly<T> &p_copy)
    {
    }

    template <typename T>
    poly<T>::poly(mat<T> A, vec<T> b)
    {
    }

    template <typename T>
    poly<T>::poly(mat<T> V)
    {
    }

    template <typename T>
    mat<T> &poly<T>::vertices()
    {
    }

    template <typename T>
    mat<T> &poly<T>::inequalities()
    {
    }

}
#endif  //__poly_h__


