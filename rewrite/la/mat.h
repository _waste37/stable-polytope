#ifndef __mat_h__
#define __mat_h__

#include "vec.h"
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstring>
#include <iostream>
#include <limits>

namespace la {
    template <typename T>
    class mat {
      public:

        mat();
        mat(const mat<T> &m_copy);
        mat(std::initializer_list<std::initializer_list<T>> list);
        mat(size_t n_rows, size_t n_cols);
        mat(size_t n_rows, size_t n_cols, T *in_ptr);
        ~mat();

        size_t rows() const;
        size_t cols() const;

        void append_row(vec<T> row);
        void remove_row(size_t index);
        void append_col(vec<T> col);

        static mat<T> id(size_t sz);
        static mat<T> zero(size_t row, size_t col);

        mat<T> transpose() const;
        bool resize(size_t n_rows, size_t n_cols, bool wipe = true);
        T &at(size_t i, size_t j);

        bool compare(const mat<T> &rhs, T tolerance);
        bool operator==(const mat<T> &rhs);
        vec<T> operator[](size_t i);
        mat<T> operator-() const;
        mat<T> operator+() const;
        mat<T> operator=(const mat<T> &m);

        template <typename U> friend U det(const mat<U> &m);
        template <typename U> friend mat<U> inverse(const mat<U> &m);
        template <typename U> friend size_t rank(const mat<U> &m);

        template <typename U> friend mat<U> operator+(const mat<U> &lhs, const mat<U> &rhs);
        template <typename U> friend mat<U> operator+(const U &lhs, const mat<U> &rhs);
        template <typename U> friend mat<U> operator+(const mat<U> &lhs, const U &rhs);

        template <typename U> friend mat<U> operator-(const mat<U> &lhs, const mat<U> &rhs);
        template <typename U> friend mat<U> operator-(const U &lhs, const mat<U> &rhs);
        template <typename U> friend mat<U> operator-(const mat<U> &lhs, const U &rhs);

        template <typename U> friend mat<U> operator*(const mat<U> &lhs, const mat<U> &rhs);
        template <typename U> friend mat<U> operator*(const U &lhs, const mat<U> &rhs);
        template <typename U> friend mat<U> operator*(const mat<U> &lhs, const U &rhs);
        template <typename U> friend mat<U> operator*(const mat<U> &lhs, const vec<U> &rhs);
        template <typename U> friend mat<U> operator*(const vec<U> &lhs, const mat<U> &rhs);
        template <typename U> friend std::ostream &operator<<(std::ostream &s, const mat<U> &m);

        void swap_rows(size_t i1, size_t i2);
        void mult_add(size_t dest, T lhs, size_t rhs);
        void mult_row(T lhs, size_t rhs);
        mat<T> augment(const mat<T> &A) const;
        void split(mat<T> &A, mat<T> &B, size_t col) const;
        // find row with max abs value in column
        size_t max_val_row(size_t column, size_t start_row);

      private:
        T *m_data;
        size_t n_rows, n_cols, n_elem;
        bool owns;
    };


    template <typename T>
    mat<T>::mat() 
        : n_rows(1), n_cols(1), n_elem(1), owns(true)
    {
        m_data = new T[n_elem];
    }

    template <typename T>
    mat<T>::mat(const mat<T> &m_copy) : owns(true)
    {
        n_rows = m_copy.n_rows;
        n_cols = m_copy.n_cols;
        n_elem = m_copy.n_elem;
        m_data = new T[n_elem];
        std::memcpy(m_data, m_copy.m_data, n_elem * sizeof(T));
    }

    template <typename T>
    mat<T>::mat(std::initializer_list<std::initializer_list<T>> list) : owns(true)
    {
        assert(list.size() > 0);
        n_rows = list.size();
        n_cols = list.begin()->size();
        n_elem = n_rows * n_cols;
        m_data = new T[n_elem];

        size_t i = 0, j;
        for (const auto &row : list) {
            assert(row.size() == n_cols);
            j = 0;
            for (const auto a : row) {
                m_data[(i * n_cols) + j] = a;
                j++;
            }
            i++;
        }
    }

    template <typename T>
    mat<T>::mat(size_t n_rows, size_t n_cols)
        : n_rows(n_rows), n_cols(n_cols), owns(true)
    {
        n_elem = n_rows * n_cols;
        m_data = new T[n_elem];
    }

    template <typename T>
    mat<T>::mat(size_t n_rows, size_t n_cols, T *in_ptr) 
        : n_rows(n_rows), n_cols(n_cols), m_data(in_ptr), owns(false)
    {
        n_elem = n_rows * n_cols;
    }

    template <typename T>
    mat<T>::~mat()
    {
        if (owns) { delete[] m_data; }
    }

    template <typename T>
    size_t mat<T>::rows() const
    {
        return n_rows;
    }

    template <typename T>
    size_t mat<T>::cols() const
    {
        return n_cols;
    }

    template <typename T>
    bool mat<T>::resize(size_t n_rows, size_t n_cols, bool wipe)
    {
        this->n_rows = n_rows;
        this->n_cols = n_cols;
        n_elem = n_rows * n_cols;
        T *temp;
        if (!wipe) {
            temp = new T[n_elem];
            std::memcpy(temp, m_data, sizeof(T) * n_elem);
        }

        delete [] m_data;
        if (wipe) {
            m_data = new T[n_elem];
        } else {
            m_data = temp;
        }

        return m_data != nullptr;
    }

    template <typename T>
    T &mat<T>::at(size_t i, size_t j)
    {
        assert(i >= 0 && i < n_rows && j >= 0 && j < n_cols);
        return m_data[(i * n_cols) + j];
    }

    template <typename T>
    vec<T> mat<T>::operator[](size_t i)
    {
        assert(i >= 0 && i < n_rows);
        return vec<T>(n_cols, m_data + (i * n_cols));
    }

    template <typename T>
    bool mat<T>::operator==(const mat<T> &rhs)
    {
        if (rhs.n_cols != n_cols || rhs.n_rows != n_rows) return false;
        for (size_t i = 0; i < n_elem; ++i) {
            if (std::abs(m_data[i] - rhs.m_data[i]) >= 1e-9) {
                return false;
            }
        }
        return true;
    }

    template <typename T>
    bool mat<T>::compare(const mat<T> &rhs, T tolerance) 
    {
        if (rhs.n_cols != n_cols || rhs.n_rows != n_rows) return false;
        T cumulative_sum = 0;
        for (size_t i = 0; i < n_elem; ++i) {
            cumulative_sum += (m_data[i]-rhs.m_data[i]) * (rhs.m_data[i]-m_data[i]);
        }

        T final_value = std::sqrt(cumulative_sum / ((n_cols * n_rows)-1));
        return final_value < tolerance;
    }

    template <typename T>
    mat<T> mat<T>::operator-() const
    {
        mat<T> res(n_rows, n_cols);
        for (size_t i = 0; i < n_elem; ++i) {
            res.m_data[i] = -m_data[i];
        }
        return res;
    }

    template <typename T>
    mat<T> mat<T>::operator+() const
    {
        return mat<T>(*this);
    }

    template <typename T>
    mat<T> mat<T>::operator=(const mat<T> &m)
    {
        if (n_cols != m.n_cols || n_rows != m.n_rows) {
            resize(m.n_rows, m.n_cols);
        } else if (m_data != nullptr) {
            n_cols = m.n_cols;
            n_rows = m.n_rows;
            n_elem = m.n_elem;
            m_data = new T[n_elem];
        }

        std::memcpy(m_data, m.m_data, n_elem * sizeof(T));
        return *this;
    }

    template <typename T> 
    mat<T> operator+(const mat<T> &lhs, const mat<T> &rhs)
    {
        assert(lhs.n_cols == rhs.n_cols && lhs.n_rows == rhs.n_rows);
        mat<T> res(lhs.n_rows, lhs.n_cols);

        for (size_t i = 0; i < lhs.n_elem; ++i) {
            res.m_data[i] = lhs.m_data[i] + rhs.m_data[i];
        }

        return res;
    }

    template <typename T> 
    mat<T> operator+(const T &lhs, const mat<T> &rhs)
    {
        mat<T> res(rhs.n_rows, rhs.n_cols);
        for (size_t i = 0; i < rhs.n_elem; ++i) {
            res.m_data[i] = lhs + rhs.m_data[i];
        }

        return res;
    }

    template <typename T> 
    mat<T> operator+(const mat<T> &lhs, const T &rhs)
    {
        return rhs + lhs;
    }

    template <typename T> 
    mat<T> operator-(const mat<T> &lhs, const mat<T> &rhs)
    {
        return lhs + (-rhs);
    }

    template <typename T> 
    mat<T> operator-(const T &lhs, const mat<T> &rhs)
    {
        return lhs + (-rhs);
    }

    template <typename T> 
    mat<T> operator-(const mat<T> &lhs, const T &rhs)
    {
        return lhs + (-rhs);
    }
    // n, m  x  m, t  n, t

    template <typename T> 
    mat<T> operator*(const mat<T> &lhs, const mat<T> &rhs)
    {
        assert(lhs.n_cols == rhs.n_rows);

        size_t temp, n = lhs.n_rows, m = lhs.n_cols, t = rhs.n_cols;
        mat<T> res(n, t);
        for (size_t i = 0; i < n; ++i) {
            for (size_t j = 0; j < t; ++j) {
                temp = 0;
                for (size_t k = 0; k < lhs.n_cols; ++k) {
                    temp += lhs.m_data[(i * m) + k] * rhs.m_data[(k * m) + j];
                }
                res.m_data[(i * t) + j] = temp;
            }
        }
        return res;
    }

    template <typename T> 
    mat<T> operator*(const T &lhs, const mat<T> &rhs)
    {
        mat<T> res(lhs.n_rows, lhs.n_cols);
        for (size_t i = 0; i < rhs.n_elem; ++i) {
            res.m_data[i] = lhs * rhs.m_data[i];
        }
        return mat<T>(rhs.n_rows, rhs.n_rows, res);
    }

    template <typename T> 
    mat<T> operator*(const mat<T> &lhs, const T &rhs)
    {
        return rhs * lhs;
    }

    template <typename T> 
    std::ostream &operator<<(std::ostream &os, const mat<T> &m)
    {
        os << '{';
        for (size_t i = 0; i < m.n_rows; ++i) {
            os << '{';
            for (size_t j = 0; j < m.n_cols; ++j) {
                os << m.m_data[(i * m.n_cols) + j];
                if (j < m.n_cols - 1) { os << ", "; }
            }
            os << '}';
            if (i < m.n_rows - 1) { os << ','; }
        }
        os << '}';
        return os;
    }

    template <typename T> 
    mat<T> mat<T>::id(size_t sz)
    {
        mat<T> id(sz, sz);
        for (size_t i = 0; i < id.n_cols; ++i) {
            for (size_t j = 0; j < sz; ++j) {
                id.m_data[(i * sz) + j] = (i == j);
            }
        }
        return id;
    }

    template <typename T> 
    mat<T> mat<T>::zero(size_t row, size_t col)
    {
        mat<T> z(row, col);
        for (size_t i = 0; i < row * col; ++i) {
            z.m_data[i] = 0;
        }
        return z;
    }

    template <typename T>
    void mat<T>::swap_rows(size_t i1, size_t i2)
    {
        T *tmp = new T[n_cols];
        std::memcpy(tmp, m_data + (i1*n_cols), n_cols * sizeof(T));
        std::memcpy(m_data + (i1*n_cols), m_data + (i2*n_cols), n_cols * sizeof(T));
        std::memcpy(m_data + (i2*n_cols), tmp, n_cols * sizeof(T));
        delete [] tmp;
    }

    template <typename T>
    void mat<T>::mult_add(size_t dest, T lhs, size_t rhs)
    {
        (*this)[dest] = lhs * (*this)[rhs];
    }

    template <typename T>
    void mat<T>::mult_row(T lhs, size_t rhs)
    {
        (*this)[rhs] = lhs * (*this)[rhs];
    }

    template <typename T>
    mat<T> mat<T>::augment(const mat<T> &A) const
    {
        size_t cols = n_cols + A.n_cols;
        mat<T> B(n_rows, cols);

        for (size_t i = 0; i < n_rows; ++i) {
            for (size_t j = 0; j < cols; ++j) {
                if (j < n_cols) {
                    B.m_data[(i * cols) + j] = m_data[(i * n_cols) + j];
                } else {
                    B.m_data[(i * cols) + j] = A.m_data[(i * A.n_cols) + (j - n_cols)];
                }
            }
        }

        return B;
    }

    template <typename T>
    void mat<T>::split(mat<T> &A, mat<T> &B, size_t col) const
    {
        assert(col < n_cols && col >= 0);
        std::cout << n_cols << std::endl;
        A.resize(n_rows, col);
        B.resize(n_rows, n_cols - col);

        for (size_t i = 0; i < n_rows; ++i) {
            for (size_t j = 0; j < n_cols; ++j) {
                if (j < col) {
                    A.m_data[(i * col) + j] = m_data[(i * n_cols) + j];
                } else {
                    B.m_data[(i * (n_cols - col)) + j - col] = m_data[(i * n_cols) + j];
                }
            }
        }
    }

    template <typename T>
    size_t mat<T>::max_val_row(size_t column, size_t start_row)
    {
        T cur_max = 0;
        size_t res;
        for (size_t i = start_row; i < n_rows; ++i) {
            if (std::abs(m_data[(i*n_cols) + column]) > std::abs(cur_max)) {
                cur_max = m_data[(i*n_cols) + column];
                std::cout << " new max =" << cur_max << std::endl;
                res = i;
            }
        }

        return res;
    }

    template <typename T>
    void mat<T>::append_row(vec<T> row)
    {
        assert(row.dim() == n_cols);
        n_rows = n_rows + 1;
        T *temp = new T[n_rows * n_cols];
        std::memcpy(temp, m_data, sizeof(T) * n_elem);
        std::memcpy(temp + n_elem, &row[0], sizeof(T) * n_cols);

        delete [] m_data;
        n_elem = n_rows * n_cols;
        m_data = temp;
    }

    template <typename T>
    void mat<T>::remove_row(size_t index)
    {
        assert(index >= 0 && index < n_rows);
        T *temp = new T[n_cols * (n_rows - 1)];
        std::memcpy(temp, m_data, sizeof(T) * n_cols * (index - 1));
        std::memcpy(temp + (n_cols * index), 
                    m_data + (n_cols * (index + 1)), 
                    sizeof(T) * n_cols * (n_rows - index)); 
        delete [] m_data;
        n_rows--;
        n_elem = n_rows * n_cols;
        m_data = temp;
    }

    template <typename T>
    void mat<T>::append_col(vec<T> col)
    {
        assert(col.dim() == n_rows);
        T *temp = new T[n_rows * (n_cols + 1)];

        for (size_t i = 0; i < n_rows; ++i) {
            std::memcpy(temp + (i * (n_cols + 1)), m_data + (i * n_cols), sizeof(T) * n_cols);
            temp[(i * (n_cols + 1)) + n_cols] = col[i];
        }
        n_cols++;
        n_elem = n_cols * n_rows;
        m_data = temp;
    }

    // n^3 determinant
    template <typename T> 
    T det(const mat<T> &m)
    {
        // should compute determinant using gaussian elimination
        // one sholud note that:

    }

    template <typename T> 
    size_t rank(const mat<T> &m)
    {
    }

    template <typename T>
    mat<T> smith_normal_form(mat<T> &m)
    {
    }
};

#endif // __mat_h__
