#ifndef __cone_h__
#define __cone_h__

#include "poly.h"

namespace la {
    template <typename T>
    class cone {
      public:
        cone();
        cone(const cone<T> &c_copy);
        cone(const poly<T> &p_ext);
        cone(const mat<T> &A);
        poly<T> project();

        mat<T> &inequalities(); // aka A s.t. cone = {x : Ax >= 0}
        mat<T> &extreme_rays(); // aka generators of cone
      private:
        mat<T> A;
        mat<T> R;
    };
}

#endif // __cone_h__
