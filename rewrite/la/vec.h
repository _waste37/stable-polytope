#ifndef __vec_h__
#define __vec_h__

#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstring>
#include <iostream>

namespace la {
    template <typename T>
    class vec {
      public:
        vec();
        vec(const vec &v_copy);
        vec(std::initializer_list<T> list);
        vec(size_t n_dim);
        // when using this method the vec does not own memory, and does not free it
        vec(size_t n_dim, T *in_ptr);
        ~vec();

        size_t dim() const;
        bool resize(size_t n_dim);
        T &at(size_t i);

        bool compare(const vec<T> &rhs, T tolerance);

        bool operator==(const vec<T> &rhs);
        T &operator[](size_t i);
        vec<T> operator-() const;
        vec<T> operator+() const;
        vec<T> operator+(const vec<T> &rhs) const;
        vec<T> operator-(const vec<T> &rhs) const;
        vec<T> operator*(const T &rhs) const;

        vec<T> operator=(const vec<T> &rhs);

        template <typename U> friend U dot(const vec<U> &v1, const vec<U> &v2);
        template <typename U> friend U cross(const vec<U> &v1, const vec<U> &v2);
        template <typename U> friend U norm(const vec<U> &m);
        template <typename U> friend vec<U> normalize(const vec<U> &v);

        template <typename U> friend vec<U> operator*(const U &lhs, const vec<U> &rhs);
        template <typename U> friend std::ostream &operator<<(std::ostream &s, const vec<U> &v);
      private:
        size_t n_dim;
        T *v_data;
        bool owns;
    };

    template <typename T>
    vec<T>::vec() : n_dim(1), owns(true)
    {
        v_data = new T[n_dim];
    }

    template <typename T>
    vec<T>::vec(const vec &v_copy) : owns(true)
    {
        n_dim = v_copy.n_dim;
        v_data = new T[n_dim];
        std::memcpy(v_data, v_copy.v_data, n_dim * sizeof (T));
    }

    template <typename T>
    vec<T>::vec(std::initializer_list<T> list) : owns(true)  
    {
        n_dim = list.size();
        v_data = new T[n_dim];

        size_t i = 0;
        for (const T a : list) {
            v_data[i] = a;
            i++;
        }
    }

    template <typename T>
    vec<T>::vec(size_t n_dim) : n_dim(n_dim), owns(true)
    {
        v_data = new T[n_dim];
    }

    template <typename T>
    vec<T>::vec(size_t n_dim, T *in_ptr) 
        : n_dim(n_dim), v_data(in_ptr), owns(false) {}

    template <typename T>
    vec<T>::~vec()
    {
        if (owns) { delete [] v_data; }
    }

    template <typename T>
    size_t vec<T>::dim() const
    {
        return n_dim;
    }

    template <typename T>
    bool vec<T>::resize(size_t n_dim)
    {
        delete [] v_data;
        this->n_dim = n_dim;
        v_data = new T[n_dim];
    }

    template <typename T>
    T &vec<T>::at(size_t i)
    {
        return v_data[i];
    }

    template <typename T>
    bool vec<T>::compare(const vec<T> &rhs, T tolerance)
    {
        if (rhs.n_dim != n_dim) return false;
        T cumulative_sum = 0;
        for (size_t i = 0; i < n_dim; ++i) {
            cumulative_sum += (rhs.v_data[i] - v_data[i]) * (v_data[i] - rhs.v_data[i]);
        }

        T final_value = std::sqrt(cumulative_sum / (n_dim - 1));
        return final_value < tolerance;
    }

    template <typename T>
    bool vec<T>::operator==(const vec<T> &rhs)
    {
        if (rhs.n_dim != n_dim) return false;
        for (size_t i = 0; i < n_dim; ++i) {
            if (std::abs(rhs.v_data[i] - v_data[i]) >= 1e-9) {
                return false;
            }
        }

        return true;
    }

    template <typename T>
    T &vec<T>::operator[](size_t i)
    {
        return v_data[i];
    }

    template <typename T>
    vec<T> vec<T>::operator-() const
    {
        vec<T> v(n_dim);
        for (size_t i = 0; i < n_dim; ++i) {
            v.v_data[i] = - v_data[i];
        }
    }

    template <typename T>
    vec<T> vec<T>::operator+() const
    {
        return vec<T>(*this);
    }

    template <typename T>
    vec<T> vec<T>::operator+(const vec<T> &rhs) const
    {
        assert(n_dim == rhs.n_dim);
        vec<T> v(n_dim);
        for (size_t i = 0; i < n_dim; ++i) {
            v.v_data[i] = v_data[i] + rhs.v_data[i];
        }
        return v;
    }

    template <typename T>
    vec<T> vec<T>::operator-(const vec<T> &rhs) const
    {
        return *this + (-rhs);
    }

    template <typename T>
    vec<T> vec<T>::operator=(const vec<T> &rhs) 
    {
        if (n_dim != rhs.n_dim) {
            resize(rhs.n_dim);
        } else if (v_data == nullptr) {
            n_dim = rhs.n_dim;
            v_data = new T[n_dim];
        }

        std::memcpy(v_data, rhs.v_data, n_dim * sizeof(T));
        return *this;
    }

    template <typename T>
    vec<T> vec<T>::operator*(const T &rhs) const
    {
        vec<T> v(n_dim);
        for (size_t i = 0; i < n_dim; ++i) {
            v.v_data[i] = v_data[i] * rhs;
        }
        return v;
    }

    template <typename T> 
    vec<T> operator*(const T &lhs, const vec<T> &rhs)
    {
        return rhs * lhs;
    }


    template <typename T> 
    T dot(const vec<T> &v1, const vec<T> &v2)
    {
        assert(v1.n_dim == v2.n_dim);
        T res = 0;
        for (size_t i = 0; i < v1.n_dim; ++i) {
            res += v1.v_data[i] * v2.v_data[i];
        }
        return res;
    }
    template <typename T> 
    T cross(const vec<T> &v1, const vec<T> &v2)
    {
        assert(v1.n_dim == v2.n_dim && v1.n_dim == 3);
        return vec<T>{
            (v1.v_data[1] * v2.v_data[2]) - (v1.v_data[2] * v2.v_data[1]),
            (v1.v_data[0] * v2.v_data[2]) - (v1.v_data[2] * v2.v_data[0]),
            (v1.v_data[0] * v2.v_data[1]) - (v1.v_data[1] * v2.v_data[0])
        };
    }

    template <typename T> 
    T norm(const vec<T> &v)
    {
        return std::sqrt(dot(v, v));
    }

    template <typename T> 
    vec<T> normalize(const vec<T> &v)
    {
        return (1 / norm(v)) * v;
    }

    template <typename T> 
    std::ostream &operator<<(std::ostream &os, const vec<T> &v)
    {
        os << '{';
        for (size_t j = 0; j < v.n_dims; ++j) {
            os << v.v_data[j];
            if (j < v.n_dims - 1) { os << ", "; }
        }
        os << '}';
        return os;
    }
}
#endif // __vec_h__
