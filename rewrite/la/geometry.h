#ifndef __geometry_h__
#define __geometry_h__

#include "poly.h"

namespace la {
    template <typename T>
    class geometry {
      public:
        static mat<T> conv_of_intpoints(mat<T> &P, vec<T> &b);
        // A is input matrix
        // U, V the results (V the extreme rays, U the generators of the subspace.
        // row is the row to which the process has stopped
        // Q is the work matrix when the process stopped.
        static void ddm(const mat<T> &A, mat<T> &U, mat<T> &V);
        static void ddm_dyn(const mat<T> &A, mat<T> &U, mat<T> &V, size_t row, mat<T> &Q);
      private:
        static void ddm_normalize(vec<T> &v);
        static std::vector<size_t> ddm_adjacent(mat<T> &Q, std::vector<size_t> &J_p, std::vector<size_t> &J_m);
    };

    template <typename T>
    void geometry<T>::ddm(const mat<T> &A, mat<T> &U, mat<T> &V)
    {
        mat<T> Q(0, A.cols());
        V = mat<T>(0, A.cols());
        U = mat<T>::id(A.cols());

        ddm_dyn(A, U, V, 0, Q);
    }

    template <typename T>
    void geometry<T>::ddm_dyn(const mat<T> &A, mat<T> &U, mat<T> &V, size_t row, mat<T> &Q)
    {
        for (size_t i = row; i < A.rows(); ++i) {
            vec<T> p = U * A.transpose()[i];
            vec<T> q = V * A.transpose()[i];

            if (p == vec<T>::zero(p.dim())) {
                std::vector<size_t> J_m, J_p;
                for (size_t j = 0; j < q.dim(); ++j) {
                    if (q[j] > 0) { J_p.push_back(j); }
                    if (q[j] < 0) { J_m.push_back(j); }
                }

                mat<T> V_new = la::mat<T>::zero(0, A.cols());
                mat<T> Q_new = la::mat<T>::zero(0, A.rows());
                std::vector<std::pair<size_t, size_t>> E = ddm_adjacent(Q, J_p, J_m);

                for (const auto &[j1, j2] : E) {

                }
            } else {

            }
        }
    }
}
#endif //__geometry_h__
