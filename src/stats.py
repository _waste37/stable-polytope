#!/usr/bin/env python3

import argparse
import stab_gen
import json
from graph_dataset import Dataset

parser = argparse.ArgumentParser()
parser.add_argument('mode', nargs=1, help='one of classes, density, dynamic, nodes, facets')

parser.add_argument('-i', '--input',
                    nargs=1,
                    required=True,
                    help='output folder from gen-graphs.py',
                    metavar='<folder>')

parser.add_argument('-o', '--output',
                    default='output',
                    help='statistics output file')

parser.add_argument('-f', '--format',
                    default='json',
                    help='statistics output file')
if __name__ == "__main__":
    args = parser.parse_args()
    output_file = args.output
    input_dir = args.input[0]
    mode = args.mode[0]
    format = args.format
    
    dataset = Dataset(input_dir)
    if mode == 'classes':
        classes = dataset.inequality_classes()
        with open(f'{output_file}.{format}', 'w') as f:
            if format == 'json':
                f.write(dataset.jformat(json.dumps(classes, indent=2)))
            elif format == 'tex':

                f.write('%rank\n')
                for obj in classes:
                    f.write(f'{obj["vertices"]}')
                    for val in obj['rank']:
                        f.write(f' & {val} ')
                    f.write(f'\\\\\n\\hline\n')

                f.write('%clique\n')
                for obj in classes:
                    f.write(f'{obj["vertices"]}')
                    for val in obj['clique']:
                        f.write(f' & {val} ')
                    f.write(f'\\\\\n\\hline\n')

                f.write('%nonrank\n')
                for obj in classes:
                    f.write(f'{obj["vertices"]}')
                    for val in obj['nonrank']:
                        f.write(f' & {val} ')
                    f.write(f'\\\\\n\\hline\n')

                f.write('%gt2\n')
                for obj in classes:
                    f.write(f'{obj["vertices"]}')
                    for val in obj['gt2']:
                        f.write(f' & {val} ')
                    f.write(f'\\\\\n\\hline\n')
            else:
                print(f'Unsupported format {format}')
                exit(-1)
    elif mode == 'density':
        densities = dataset.inequality_density()
        with open(f'{output_file}.{format}', 'w') as f:
            if format == 'json':
                f.write(dataset.jformat(json.dumps(densities, indent=2)))
            elif format == 'tex':
                f.write('%avg density\n')
                for obj in densities:
                    f.write(f'{obj["vertices"]}')
                    for d in obj['density']:
                        f.write(f' & {d}')
                    f.write(f'\\\\\n\\hline\n')
            else:
                print(f'Unsupported format {format}')
                exit(-1)
    elif mode == 'dynamic':
        dynamics = dataset.inequality_dynamic()
        with open(f'{output_file}.{format}', 'w') as f:
            if format == 'json':
                f.write(dataset.jformat(json.dumps(dynamics, indent=2)))
            elif format == 'tex':
                f.write('%average max dynamic\n')
                for obj in dynamics:
                    f.write(f'{obj["vertices"]}')
                    for d in obj['dynamic']:
                        f.write(f' & {d}')
                    f.write(f'\\\\\n\\hline\n')
            else:
                print(f'Unsupported format {format}')
                exit(-1)
    elif mode == 'nodes':
        nodes = dataset.bc_nodes()
        with open(f'{output_file}.{format}', 'w') as f:
            if format == 'json':
                f.write(dataset.jformat(json.dumps(nodes, indent=2)))
            elif format == 'tex':
                f.write('%branch bound nodes\n')
                for obj in nodes:
                    f.write(f'{obj["vertices"]}')
                    for d in obj['edge_nodes']:
                        f.write(f' & {d}')
                    f.write(f'\\\\\n\\hline\n')
            else:
                print(f'Unsupported format {format}')
                exit(-1)
    elif mode == 'facets':
        dataset.facet_number()

        facets = dataset.facet_number()
        with open(f'{output_file}.{format}', 'w') as f:
            if format == 'json':
                f.write(dataset.jformat(json.dumps(facets, indent=2)))
            elif format == 'tex':
                f.write('%branch bound nodes\n')
                for obj in facets:
                    f.write(f'{obj["vertices"]}')
                    for d in obj['facets']:
                        f.write(f' & {d}')
                    f.write(f'\\\\\n\\hline\n')
            else:
                print(f'Unsupported format {format}')
                exit(-1)
    else:
        print(f'unknown action {mode}')
        exit(-1)
