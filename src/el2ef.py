import argparse
import networkx as nx
import stab_gen

parser = argparse.ArgumentParser()
parser.add_argument('input', nargs=1, help='input edge list file')
parser.add_argument('-o', '--output', default='output', help='output filename')
parser.add_argument('-t', '--threads', default=8, help='set number of threads')

if __name__ == '__main__':
    args = parser.parse_args()
    output = args.output
    input = args.input[0]
    t_count = args.threads
    g = stab_gen.load_graph(input)
    print(f'Graph {input} has {len(g)} vertices and {len(g.edges)} edges')

    stab_gen.thread_number(t_count)
    print(f'Set PyNormaliz thread number to {t_count}')

    g.atoms = []
    for component in nx.connected_components(g):
        h = g.subgraph(component)
        dec = stab_gen.clique_decomposition(h)
        g.atoms += dec
    print(f'Input graph decomposed into {len(g.atoms)} atoms')

    stab_gen.compute_facets_decomposition_tree(output, g, True)
    print('Done!')
