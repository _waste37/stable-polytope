#!/usr/bin/env python3

import networkx as nx
import sys

def adjacent(g, u, x):
    return u in g.neighbors(x)

def find_z(g, x, y):
    for u in g:
        if u == x or u == y:
            continue
        for v in g:
            if adjacent(g, u, v) or v == x or v == y or v == u:
                continue;

            if not (adjacent(g, u, x) and adjacent(g, u, y)):
                return u

            if not (adjacent(g, v, x) and adjacent(g, v, y)):
                return v

            return u

def rule1(S, T, L1, u, x, y):
    removal = []
    applied = False
    for v in T:
        if g.has_edge(u, y) and g.has_edge(x, v) and not g.has_edge(u, v):
            applied = True
            removal.append(v)
            L1.append(v)
            S.add(v)
    for v in removal:
        T.remove(v)
    return applied 

def rule2(S, T, L1, u, x, y):
    removal = []
    applied = False
    for v in T:
        if g.has_edge(u, v) and not g.has_edge(x, v):
            applied = True
            removal.append(v)
            L1.append(v)
            S.add(v)
    for v in removal:
        T.remove(v)
    return applied 

def rule3(S, K, T, L1, L2, u, x, y):
    removal = []
    applied = False
    for v in T:
        if g.has_edge(u, v) and not g.has_edge(u, y):
            applied = True
            removal.append(v)
            if g.has_edge(x, v) and g.has_edge(v, y):
                L2.append(v)
                K.add(v)
            else:
                L1.append(v)
                S.add(v)
    for v in removal:
        T.remove(v)
    return applied 

def rule4(S, K, L1, u, x, y):
    removal = []
    applied = False
    for v in K:
        if g.has_edge(u, y) and not g.has_edge(u, v):
            applied = True
            L1.append(v)
            S.add(v)
            removal.append(v)

    for v in removal:
        K.remove(v)
    return applied

def rule5(S, T, L1, u, x, y):
    removal = []
    applied = False
    for v in T:
        if g.has_edge(x, v) and not g.has_edge(u, v):
            applied = True
            L1.append(v)
            S.add(v)
            removal.append(v)

    for v in removal:
        T.remove(v)
    return applied

def rule6(S, K, L1, u, x, y):
    removal = []
    applied = False
    for v in K:
        if v != u and not g.has_edge(u, v):
            applied = True
            L1.append(v)
            S.add(v)
            removal.append(v)

    for v in removal:
        K.remove(v)
    return applied


def amalgam(g, x, y):
    z = find_z(g, x, y)

    L1 = [x, z]
    L2 = []

    S = set(L1)
    K = set()
    T = set([u for u in g if u != x and u != z])
    print(S, K, T)

    while len(L1) != 0 or len(L2) != 0:
        some_rule_applied = False
        if len(L1) != 0:
            # scan a node in L1
            u = L1.pop()
            some_rule_applied = some_rule_applied or rule1(S, T, L1, u, x, y)
            some_rule_applied = some_rule_applied or rule2(S, T, L1, u, x, y)
            some_rule_applied = some_rule_applied or rule3(S, K, T, L1, L2, u, x, y)
            some_rule_applied = some_rule_applied or rule4(S, K, L1, u, x, y)

        if len(L2) != 0:
            u = L2.pop()
            some_rule_applied = some_rule_applied or rule5(S, T, L1, u, x, y)
            some_rule_applied = some_rule_applied or rule6(S, K, L1, u, x, y)

    return (S, K, T)

def amalgam_cubic(g, x, y):
    z = find_z(g, x, y)
    S = set([x,z])
    K = set()
    T = set([u for u in g if u != x and u != z])

    while True:
        rules_applied = False
        for u in g:
            for v in g:
                if u  == v:
                    continue
                # rule1
                if u in S and v in T and g.has_edge(u,y) and g.has_edge(x,v) and not g.has_edge(u,v):
                    rules_applied = True
                    S.add(v)
                    T.remove(v)
                # rule 2
                if u in S and v in T and g.has_edge(u,v) and not g.has_edge(x,v):
                    rules_applied = True
                    S.add(v)
                    T.remove(v)
                # rule 3
                if u in S and v in T and g.has_edge(u,v) and not g.has_edge(u,y):
                    rules_applied = True
                    T.remove(v)
                    if g.has_edge(x,v) and g.has_edge(v,y):
                        K.add(v)
                    else:
                        S.add(v)
                # rule 4
                if u in S and v in K and g.has_edge(u,y) and not g.has_edge(u,v):
                    rules_applied = True
                    S.add(v)
                    K.remove(v)
                # rule 5
                if u in K and v in T and g.has_edge(x,v) and not g.has_edge(u,v):
                    rules_applied = True
                    S.add(v)
                    T.remove(v)
                # rule 6
                if u in K and v in K and u != v and not g.has_edge(u,v):
                    rules_applied = True
                    S.add(v)
                    K.remove(v)
        if not rules_applied:
            return (S, K, T)

def print_graph(g):
    for u in g:
        print(u, ':', list(g[u]))

if __name__ == '__main__':
    p = 0.3
    n = 30
    amalgam_count = 0
    iterations = 1000
    for i in range(iterations):
        print(i)
        g = nx.erdos_renyi_graph(n, p)
        while not nx.is_connected(g):
            g = nx.erdos_renyi_graph(n, p)

        for (u,v) in g.edges():
            (S,K,T) = amalgam_cubic(g, u, v)
            if len(T) >= 2:
                break
        if len(T) < 2:
            print('No amalgam')
        else:
            amalgam_count += 1
            print('Amalgam')
            print(S, K, T)
    print(f'G({n},{p}) amalgam probability:', amalgam_count / iterations)
