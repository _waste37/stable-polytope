#!/usr/bin/env python3 

import json
import time
import os
import sys
import random
import itertools

import networkx as nx
import PyNormaliz as nmz
import numpy as np

def thread_number(number=8):
    nmz.NmzSetNumberOfNormalizThreads(number)

def load_graph(path) -> nx.Graph:
    g = nx.Graph()
    with open(path, 'r') as f:
        for line in f:
            tmp = line.rstrip().split(' ')
            g.add_edge(int(tmp[0]) - 1, int(tmp[1]) - 1)
    return g


def dump(path, g):
    os.makedirs(path, exist_ok=True)
    with open(f'{path}/e_list', 'w') as f:
        for (u, v) in g.edges():
            f.write(f'{u} {v}\n')


def format_equation(plane):
    printed = False
    result = ''
    for i in range(1, len(plane)):
        if plane[i-1] == 0:
            continue

        if plane[i-1] == 1:
            if printed:
                result += '+ '
            result += f'x{i} '
        elif plane[i-1] == -1:
            result += f'- x{i} '
        else:
            if plane[i-1] > 0 and printed:
                result += '+ '
            result += f'{plane[i-1]} x{i} '
        printed = True
    result += f'>= {-plane[len(plane)-1]}\n'
    return result


def constraints(planes):
    result = ''
    count = 0
    for plane in planes:
        result += f'{count}: '
        result += format_equation(plane)
        count += 1
    return result


def compute_facets(path, g, human=False):
    try:
        print('Enumerating stable sets..', end='\t')
        sys.stdout.flush()
        start = time.time_ns()

        stable_sets = nx.algorithms.enumerate_all_cliques(nx.complement(g))

        end = time.time_ns()
        print(f'{(end - start) / 1000000000:.2f}s')

        integer_points = [[0] * len(g)]
        for ss in stable_sets:
            point = [0] * len(g)
            for v in ss:
                point[v] = 1
            integer_points.append(point)

        polytope = nmz.Cone(polytope = integer_points)

        print('Computing support hyperplanes..', end=' ')
        sys.stdout.flush()
        start = time.time_ns()

        planes = polytope.SupportHyperplanes()

        end = time.time_ns()
        print(f'{(end - start) / 1000000000:.2f}s')
    except KeyboardInterrupt:
        print('\nSkipped')
        return

    if human:
        with open(f'{path}/facets.human', 'w') as f:
            f.write('Facets:\n')
            f.write(constraints(planes))

    with open(f'{path}/facets.json', 'w') as f:
        json.dump({'facets': planes}, f)


def graph(n, p):
    return nx.erdos_renyi_graph(n, p)


def compute_facets_decomposition_tree(path, g, human=False):
    try:
        atom_planes = {}
        for i, atom in enumerate(g.atoms):
            atom.projection_map = {v: j for j, v in enumerate(atom)}

            print(f'Enumerating stable sets for atom {i}..', end='\t\t')
            sys.stdout.flush()
            start = time.time_ns()

            stable_sets = nx.algorithms.enumerate_all_cliques(nx.complement(atom))

            end = time.time_ns()
            print(f'{(end - start) / 1000000000:.2f}s')

            integer_points = [[0] * len(atom)]
            for ss in stable_sets:
                point = [0] * len(atom)
                for v in ss:
                    point[atom.projection_map[v]] = 1
                integer_points.append(point)

            polytope = nmz.Cone(polytope=integer_points)

            print(f'Computing support hyperplanes for atom {i}..', end='\t')
            sys.stdout.flush()
            start = time.time_ns()

            atom_planes[i] = polytope.SupportHyperplanes()

            end = time.time_ns()
            print(f'{(end - start) / 1000000000:.2f}s')

        planes = []
        for i, atom in enumerate(g.atoms):
            for p in atom_planes[i]:
                plane = [0] * (len(g) + 1)
                for v in atom:
                    plane[v] = p[atom.projection_map[v]]
                    plane[len(g)] = p[len(atom)]
                planes.append(plane)

        planes.sort()
        planes = list(x for x, _ in itertools.groupby(planes))
    except KeyboardInterrupt:
        print('\nSkipped')
        return

    if human:
        with open(f'{path}/facets.human', 'w') as f:
            f.write('Facets:\n')
            f.write(constraints(planes))

    with open(f'{path}/facets.json', 'w') as f:
        json.dump({'facets': planes}, f)


def graph_decomposition_tree(n, p, threshold=False):
    if not threshold:
        threshold = n - 3
    count = 0
    while True:
        g = nx.erdos_renyi_graph(n, p)
        g.atoms = []
        for component in nx.connected_components(g):
            h = g.subgraph(component)
            dec = clique_decomposition(h)
            is_good = True
            for atom in dec:
                if len(atom) > threshold:
                    is_good = False
                    break
            if not is_good:
                break

            g.atoms += dec

        if not is_good:
            print(f'decomposition failed: {count}', end='\r')
            count += 1
            continue

        if count > 0:
            print('')
        print(f'generated G_{n}_{p:.2f} with {len(g.atoms)} atoms')
        return g


'''
    Python implementation of LEXM algorithm by
    Rose, Tarjan, Lueker: Algorithmic Aspects of Vertex Elimination on Graphs (1976)
'''


def label_less_than(label1, label2):
    n = min(len(label1), len(label2))
    for i in range(n):
        if label1[i] < label2[i]:
            return True
        elif label1[i] > label2[i]:
            return False
    if len(label1) < len(label2):
        return True
    elif len(label1) > len(label2):
        return False
    return False


def largest_labeled_node(G, label, pi):
    cur_label = ''
    cur_node = None
    nodes = [u for u in G]
    random.shuffle(nodes)

    for node in nodes:
        if (node not in pi) and ((cur_node is None) or (label_less_than(cur_label, label[node]))):
            cur_node = node
            cur_label = label[node]
    return cur_node


def LEXM(G):
    F = []
    pi = {}
    label = {node: [] for node in G}
    unnumbered_nodes = [u for u in G if u not in pi]
    random.shuffle(unnumbered_nodes)
    for i in range(len(G), 0, -1):
        v = largest_labeled_node(G, label, pi)
        pi[v] = i
        unnumbered_nodes.remove(v)
        S = []
        for u in unnumbered_nodes:
            useful_nodes = [v, u] + [z for z in unnumbered_nodes if label_less_than(label[z], label[u])]
            if nx.has_path(G.subgraph(useful_nodes), v, u):
                S.append(u)

        for u in S:
            label[u].append(i)
            if not G.has_edge(v, u):
                F.append((v, u))

    return (F, pi)


def is_subclique(G, nodelist):
    n = len(nodelist)
    return n == 1 or G.subgraph(nodelist).size() == n*(n-1)/2


def decompose(G, C, v):
    H = G.subgraph([u for u in G.nodes() if u not in C[v]])
    components = nx.connected_components(H)

    A = [list(c) for c in components if v in c][0]
    B = [u for u in G.nodes if u not in C[v] and u not in A]
    return (A, B)


def clique_decomposition(G):
    (F, pi) = LEXM(G)

    H = nx.Graph()
    H.add_edges_from(list(G.edges()) + F)
    C = {v: [w for w in G if (pi[w] > pi[v]) and H.has_edge(v, w)] for v in G}

    pi_inverse = {v: k for k, v in pi.items()}
    decomposition = []

    # in ascending order of elimination
    for i in range(1, len(G)+1):
        v = pi_inverse[i]
        if not is_subclique(G, C[v]):
            continue
        # try decomposing G by C[v]
        (A, B) = decompose(G, C, v)

        if B:  # decomposition successful
            # print(f'{A+C[v]} and {B+C[v]}')
            decomposition.append(G.subgraph(A+C[v]).copy())
            G = G.subgraph(B+C[v]).copy()

    decomposition.append(G)
    return decomposition
