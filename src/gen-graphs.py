#!/usr/bin/env python3

# utils
import os
import argparse
import numpy as np
import networkx as nx
import stab_gen

# COMMAND LINE OPTIONS  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  #

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--min-cardinality', nargs=1, required=True,
                    type=int, help='minimum number of vertices',
                    metavar='<integer>')

parser.add_argument('-u', '--max-cardinality', nargs=1, required=True,
                    type=int, help='maximum number of vertices',
                    metavar='<integer>')

parser.add_argument('-m', '--min-density', nargs=1,
                    default=[0.2], type=float,
                    help='minimum edge density for generated graphs',
                    metavar='<float>')

parser.add_argument('-M', '--max-density', nargs=1,
                    default=[0.8], type=float,
                    help='minimum edge density for generated graphs',
                    metavar='<float>')

parser.add_argument('-t', '--threads', nargs=1,
                    default=[8], type=int,
                    help='number of threads used',
                    metavar='<number>')

parser.add_argument('-s', '--step', nargs=1, default=[0.1],
                    type=float, help='increase of probability each step')

parser.add_argument('-r', '--repetitions', nargs=1, default=[1],
                    type=int, help='repetition for each probability')

parser.add_argument('-o', '--output', default='output',
                    help='output directory')

parser.add_argument('-j', '--human', action='store_true',
                    help='dump images')

parser.add_argument('-d', '--decomposition', action='store_true',
                    help='enable clique cut set decomposition')

# GLOBAL VARIABLES  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  #

min_nodes = 0
max_nodes = 0
min_density = 0.0
max_density = 0.0
step = 0
repetitions = 0
output_dir = ''
index_path = None
produce_images = False
use_decomposition = False

# FUNCTIONS ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  #


def generate_graphs():
    graphs = []
    for n in range(min_nodes, max_nodes + 1):
        print('Generating graphs of cardinality:', n)
        for p in np.arange(min_density, max_density + 0.00001, step):
            print(f'for p = {p:.2f},')
            for i in range(repetitions):
                if use_decomposition:
                    graph = stab_gen.graph_decomposition_tree(n, p)
                else:
                    graph = stab_gen.graph(n, p)

                path = f'{output_dir}/{n}-vertices/' +\
                    f'edge-density-{p*100:.0f}/graph-{i}'

                graphs.append((path, graph))
        print('\b\b')

    return graphs


if __name__ == '__main__':
    args = parser.parse_args()

    produce_human_readable = args.human
    use_decomposition = args.decomposition

    min_nodes = args.min_cardinality[0]
    max_nodes = args.max_cardinality[0]

    min_density = args.min_density[0]
    max_density = args.max_density[0]

    step = args.step[0]
    repetitions = args.repetitions[0]

    output_dir = args.output
    stab_gen.thread_number(args.threads[0])

    if min_density < 0 or max_density > 1:
        print('Density range must lie in [0,1]!')
        exit(-1)

    if min_density > max_density:
        print('You swapped the interval bounds!')
        exit(-1)

    graphs = generate_graphs()
    for path, graph in graphs:
        stab_gen.dump(path, graph)

    print('Start computation:')
    for path, graph in graphs:
        print(f'PROCESSING: {path}')
        if use_decomposition:
            stab_gen.compute_facets_decomposition_tree(
                    path, graph, produce_human_readable)
        else:
            stab_gen.compute_facets(path, graph, produce_human_readable)
