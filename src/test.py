import stab_gen
import networkx as nx

G = nx.Graph()
G.add_edges_from([(0,2), (2,1), (0,1)])
stab_gen.compute_facets("test", G, True) 
