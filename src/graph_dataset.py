import re
import json
import os
import networkx as nx
import stab_gen


class Dataset:
    def __init__(self, path):
        self.path = path
        self.data = []
        temp_list_c = os.listdir(self.path)
        temp_list_c.sort()
        for (i, c) in enumerate(temp_list_c):
            c_dir = self.path + '/' + c
            if not os.path.isdir(c_dir):
                continue

            n = int(re.search(r'\d+', c).group())

            temp_list_d = os.listdir(c_dir)
            temp_list_d.sort()
            self.data.append((n, []))
            for (j, d) in enumerate(temp_list_d):
                d_dir = c_dir + '/' + d
                if not os.path.isdir(d_dir):
                    continue

                p = int(re.search(r'\d+', d).group())
                temp_list_g = os.listdir(d_dir)
                temp_list_g.sort()
                self.data[i][1].append((p, []))
                for g in temp_list_g:
                    g_dir = d_dir + '/' + g
                    if not os.path.isdir(g_dir):
                        continue
                    try:
                        self.data[i][1][j][1].append(GraphDataset(g_dir, n, p))
                    except:
                        continue

    def jformat(self, json_str):
        skip = False
        s = ''
        for c in json_str:
            if skip and (c == '\n' or c == ' '):
                continue
            elif c == '[':
                skip = True
            elif c ==']':
                skip = False
            s += c
        return s

    def inequality_density(self):
        result = []
        for (n, n_list) in self.data:
            json_dict = {
                    "vertices": n,
                    "p": [],
                    "density": [],
                    }
            for (p, p_list) in n_list:
                np_density = 0
                for g in p_list:
                    np_density += g.mean_density()
                json_dict["p"].append(p)
                json_dict["samples"] = len(p_list)
                json_dict["density"]\
                    .append(round(np_density / len(p_list), 2))
            result.append(json_dict)
        return result

    def inequality_dynamic(self):
        result = []
        for (n, n_list) in self.data:
            json_dict = {
                    "vertices": n,
                    "p": [],
                    "dynamic": [],
                    }
            for (p, p_list) in n_list:
                np_dynamic = 0
                for g in p_list:
                    np_dynamic += g.max_dynamic()
                json_dict["p"].append(p)
                json_dict["samples"] = len(p_list)
                json_dict["dynamic"]\
                    .append(round(np_dynamic / len(p_list), 2))
            result.append(json_dict)
        return result

    def facet_number(self):
        result = []
        for (n, n_list) in self.data:
            json_dict = {
                    "vertices": n,
                    "p": [],
                    "facets": [],
                    }
            for (p, p_list) in n_list:
                np_number = 0
                for g in p_list:
                    np_number += g.facet_count()

                json_dict["p"].append(p)
                json_dict["samples"] = len(p_list)
                json_dict["facets"]\
                    .append(round(np_number / len(p_list), 2))
            result.append(json_dict)
        return result

    def inequality_classes(self):
        result = []
        for (n, n_list) in self.data:
            json_dict = {
                    "vertices": n,
                    "p": [],
                    "rank": [],
                    "nonrank": [],
                    "clique": [],
                    "gt2": [],
                    }
            for (p, p_list) in n_list:
                rank_mean = 0
                clique_mean = 0
                nonrank_mean = 0
                big_mean = 0
                i = 0
                for g in p_list:
                    (rank, clique) = g.rank_count()
                    (nonrank, big) = g.nonrank_count()
                    i += 1
                    if rank + nonrank > 0:
                        rank_mean += (rank * 100.0) / (rank+nonrank)
                        clique_mean += (clique * 100.0) / (rank+nonrank)
                        nonrank_mean += (nonrank * 100.0) / (rank+nonrank)
                        big_mean += (big * 100.0) / (rank+nonrank)

                json_dict["p"].append(p)
                json_dict["samples"] = i
                json_dict["rank"]\
                    .append(round(rank_mean/i, 2))
                json_dict["nonrank"]\
                    .append(round(nonrank_mean / i, 2))
                json_dict["clique"]\
                    .append(round(clique_mean / i, 2))
                json_dict["gt2"]\
                    .append(round(big_mean / i, 2))
            result.append(json_dict)
        return result

    def gen_integer_formulations(self):
        for (n, n_list) in self.data:
            for (p, p_list) in n_list:
                for g in p_list:
                    if not os.path.isdir(g.path + '/formulations'):
                        os.makedirs(g.path + '/formulations')
                    with open(g.path + '/formulations/edge.lp', 'w') as f:
                        f.write(g.edge_ip())
                    with open(g.path + '/formulations/exact.lp', 'w') as f:
                        f.write(g.exact_lp())

    def gen_rank_formulations(self):
        for (n, n_list) in self.data:
            for (p, p_list) in n_list:
                for g in p_list:
                    if not os.path.isdir(g.path + '/formulations'):
                        os.makedirs(g.path + '/formulations')
                    with open(g.path + '/formulations/rank.lp', 'w') as f:
                        f.write(g.rank_lp())

    def bc_nodes(self):
        result = []
        for (n, n_list) in self.data:
            json_dict = {
                "vertices": n,
                "p": [],
                "edge_nodes": [],
            }

            for (p, p_list) in n_list:
                edge_nodes = 0
                for g in p_list:
                    nodes = g.bc_enumerated_nodes('edge.lp')
                    edge_nodes += nodes

                json_dict["p"].append(p)
                json_dict["samples"] = len(p_list)
                json_dict["edge_nodes"]\
                    .append(round(edge_nodes / len(p_list), 2))
            result.append(json_dict)
        return result

class GraphDataset:
    def __init__(self, path, n, p):
        self.path = path
        self.n = n
        self.p = p

        self.g = nx.Graph()
        with open(f'{self.path}/e_list', 'r') as f:
            for line in f:
                tmp = line.rstrip().split(' ')
                self.g.add_edge(int(tmp[0]), int(tmp[1]))

    def lazy_init(self):
        if not hasattr(self, 'facets'):
            with open(f'{self.path}/facets.json', 'r') as f:
                self.facets = json.load(f)['facets']

    def __str__(self):
        return 'G_{' + str(self.n) + ', ' + str(self.p) + '}'

    def facet_count(self):
        self.lazy_init()
        return len(self.facets)

    def __is_nonrank(self, plane):
        for i in range(0, len(plane) - 1):
            if plane[i] < -1:
                return True
        return False

    def __is_rank(self, plane):
        return not self.__is_nonrank(plane)

    def __is_clique_inequality(self, plane):
        if plane[len(plane) - 1] != 1:
            return False

        expected_clique_nodes = []
        for i in range(0, len(plane) - 1):
            if plane[i] == -1:
                expected_clique_nodes.append(i)
        return stab_gen.is_subclique(self.g, expected_clique_nodes)

    def mean_density(self):
        self.lazy_init()
        mean_density = 0.0
        for plane in self.facets:
            count = 0
            for i in range(0, len(self.g)):
                if plane[i] != 0:
                    count += 1
            mean_density += count / self.n
        return (mean_density / len(self.facets)) * 100

    def max_dynamic(self):
        self.lazy_init()
        dynamic = 0
        for plane in self.facets:
            dynamic = max(dynamic, -min(plane))

        return dynamic

    def rank_count(self):
        self.lazy_init()
        rank_inequalities = 0
        clique_inequalities = 0
        for plane in self.facets:
            if self.__is_rank(plane):
                rank_inequalities += 1
                if self.__is_clique_inequality(plane):
                    clique_inequalities += 1
        return (rank_inequalities, clique_inequalities)

    def nonrank_count(self):
        self.lazy_init()
        nonrank_inequalities = 0
        greater_than_2 = 0
        for plane in self.facets:
            if self.__is_nonrank(plane):
                nonrank_inequalities += 1
                if min(plane) < -2:
                    greater_than_2 += 1
        return (nonrank_inequalities, greater_than_2)

    def nonrank_lp(self):
        self.lazy_init()
        s = 'max '
        for i in range(1, self.n):
            s += f'x{i} + '
        s += f'x{self.n}\ns.t.\n'

        for plane in self.facets:
            if self.__is_nonrank(plane):
                s += stab_gen.format_equation(plane)

        s += '\nBounds\n'
        for i in range(1, self.n+1):
            s += f'0 <= x{i} <= 1\n'
        s += 'End\n'
        return s

    def rank_lp(self):
        self.lazy_init()
        s = 'max '
        for i in range(1, self.n):
            s += f'x{i} + '
        s += f'x{self.n}\ns.t.\n'

        for plane in self.facets:
            if self.__is_rank(plane):
                s += stab_gen.format_equation(plane)

        s += '\nBounds\n'
        for i in range(1, self.n+1):
            s += f'0 <= x{i} <= 1\n'
        s += 'End\n'
        return s

    def exact_lp(self):
        self.lazy_init()
        s = 'max '
        for i in range(1, self.n):
            s += f'x{i} + '
        s += f'x{self.n}\ns.t.\n'

        for plane in self.facets:
            s += stab_gen.format_equation(plane)

        s += '\nBounds\n'
        for i in range(1, self.n+1):
            s += '0 <= x' + str(i) + ' <= 1\n'
        s += 'End\n'

        return s

    def edge_ip(self):
        s = 'max '
        for i in range(1, self.n):
            s += f'x{i} + '
        s += f'x{self.n}\ns.t.\n'

        for u, v in self.g.edges():
            s += f'-x{u+1} - x{v+1} >= -1\n'

        s += 'Binaries\n'
        for i in range(1, self.n):
            s += f'x{i} '
        s += f'x{self.n}\nEnd\n'
        return s

    def bc_enumerated_nodes(self, problem):
        stream = os.popen(f'cbc {self.path}/formulations/{problem} -cuts off '
                          + '-heur off -preprocess off -strongbranching 0 ' +
                          'solve|tail -n7|head -n1')
        out = stream.read()
        num = out.split(' ')
        return int(num[len(num) - 1])
